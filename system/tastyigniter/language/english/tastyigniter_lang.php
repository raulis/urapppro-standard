<?php
/**
 * TastyIgniter
 *
 * An open source online ordering, reservation and management system for restaurants.
 *
 * @package   TastyIgniter
 * @author    SamPoyigi
 * @copyright TastyIgniter
 * @link      http://tastyigniter.com
 * @license   http://opensource.org/licenses/GPL-3.0 The GNU GENERAL PUBLIC LICENSE
 * @since     File available since Release 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tastyigniter_system_name']       = 'UR app PRO';
$lang['tastyigniter_system_powered']    = '<a target="_blank" href="http://urapppro.net">Powered by URappPRO</a>';
$lang['tastyigniter_copyright']         = 'Thank you for using <a target="_blank" href="http://urapppro.net">URappPRO</a>';
$lang['tastyigniter_version']           = 'Version %s';

/* End of file tastyigniter_lang.php */
/* Location: ./system/tastyigniter/language/english/tastyigniter_lang.php */