<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Admin_remote_hub_module extends Admin_Controller
{

    public function index($module = array()) {
        $this->lang->load('remote_hub_module/remote_hub_module');

        $this->user->restrict('Module.RemoteHubModule');

        if (!empty($module)) {
            $title = (isset($module['title'])) ? $module['title'] : $this->lang->line('_text_title');

            $this->template->setTitle('Module: ' . $title);
            $this->template->setHeading('Module: ' . $title);
            $this->template->setButton($this->lang->line('button_save'), array('class' => 'btn btn-primary', 'onclick' => '$(\'#edit-form\').submit();'));
            $this->template->setButton($this->lang->line('button_save_close'), array('class' => 'btn btn-default', 'onclick' => 'saveClose();'));
            $this->template->setButton($this->lang->line('button_icon_back'), array('class' => 'btn btn-default', 'href' => site_url('extensions')));

            $ext_data = array();
            if ( ! empty($module['ext_data']) AND is_array($module['ext_data'])) {
                $ext_data = $module['ext_data'];
            }

            if (isset($this->input->post['title'])) {
                $data['title'] = $this->input->post('title');
            } else if (isset($ext_data['title'])) {
                $data['title'] = $ext_data['title'];
            } else {
                $data['title'] = $title;
            }

            if (isset($this->input->post['hub_id'])) {
                $data['hub_id'] = $this->input->post('hub_id');
            } else if (isset($ext_data['hub_id'])) {
                $data['hub_id'] = $ext_data['hub_id'];
            } else {
                $data['hub_id'] = '';
            }

            if (isset($this->input->post['order_url'])) {
                $data['order_url'] = $this->input->post('order_url');
            } else if (isset($ext_data['order_url'])) {
                $data['order_url'] = $ext_data['order_url'];
            } else {
                $data['order_url'] = '';
            }

            if (isset($this->input->post['notify_url'])) {
                $data['notify_url'] = $this->input->post('notify_url');
            } else if (isset($ext_data['notify_url'])) {
                $data['notify_url'] = $ext_data['notify_url'];
            } else {
                $data['notify_url'] = '';
            }

            if (isset($this->input->post['api_key'])) {
                $data['api_key'] = $this->input->post('api_key');
            } else if (isset($ext_data['api_key'])) {
                $data['api_key'] = $ext_data['api_key'];
            } else {
                $data['api_key'] = '';
            }

            if (isset($this->input->post['api_password'])) {
                $data['api_password'] = $this->input->post('api_password');
            } else if (isset($ext_data['api_password'])) {
                $data['api_password'] = $ext_data['api_password'];
            } else {
                $data['api_password'] = '';
            }

            if ($this->input->post() AND $this->_updateModule() === TRUE) {
                if ($this->input->post('save_close') === '1') {
                    redirect('extensions');
                }

                redirect('extensions/edit/module/remote_hub_module');
            }

            return $this->load->view('remote_hub_module/admin_remote_hub_module', $data, TRUE);
        }
    }

    private function _updateModule()
    {
        $this->user->restrict('Module.RemotePrintModule.Manage');

        if ($this->validateForm() === TRUE) {

            if ($this->Extensions_model->updateExtension('module', 'remote_hub_module', $this->input->post())) {
                $this->alert->set('success', sprintf($this->lang->line('alert_success'), $this->lang->line('_text_title') . ' module ' . $this->lang->line('text_updated')));
            } else {
                $this->alert->set('warning', sprintf($this->lang->line('alert_error_nothing'), $this->lang->line('text_updated')));
            }

            return TRUE;
        }
    }

    private function validateForm()
    {
        $this->form_validation->set_rules('title', 'lang:label_title', 'xss_clean|trim|required|min_length[2]|max_length[128]');
        $this->form_validation->set_rules('hub_id', 'lang:label_hub_id', 'xss_clean|trim|required');
        $this->form_validation->set_rules('api_key', 'lang:label_api_key', 'xss_clean|trim|required');
        $this->form_validation->set_rules('api_password', 'lang:label_api_password', 'xss_clean|trim|required');
        $this->form_validation->set_rules('order_url', 'lang:label_order_url', 'xss_clean|trim|required');
        $this->form_validation->set_rules('notify_url', 'lang:label_notify_url', 'xss_clean|trim|required');

        if ($this->form_validation->run() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

/* End of file remote_hub_module.php */
/* Location: ./extensions/remote_hub_module/controllers/remote_hub_module.php */