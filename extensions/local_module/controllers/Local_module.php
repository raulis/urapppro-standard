<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Local_module extends Main_Controller {

	public function __construct() {
		parent::__construct(); 																	// calls the constructor
		$this->load->library('location'); 														// load the location library
		$this->location->initialize();
		$this->load->model('Addresses_model'); 
		$this->load->library('country');
		$this->load->library('currency'); 														// load the location library
		$this->lang->load('local_module/local_module');

		$referrer_uri = explode('/', str_replace(site_url(), '', $this->agent->referrer()));
		$this->referrer_uri = (!empty($referrer_uri[0]) AND $referrer_uri[0] !== 'local_module') ? $referrer_uri[0] : 'home';
	}
	
	public function index($module = array()) {
		if ( ! file_exists(EXTPATH .'local_module/views/local_module.php')) { 								//check if file exists in views folder
			show_404(); 																		// Whoops, show 404 error page!
		}

		$ext_data = (!empty($module['data']) AND is_array($module['data'])) ? $module['data'] : array();

		if (empty($module['status']) OR (isset($ext_data['status']) AND $ext_data['status'] !== '1')) {
			return;
		}

		$this->template->setStyleTag(extension_url('local_module/views/stylesheet.css'), 'local-module-css', '100000');

		$data['location_search_mode'] = 'multi';
		if (isset($ext_data['location_search_mode']) AND $ext_data['location_search_mode'] === 'single') {
			$data['location_search_mode'] = 'single';

			if (!empty($ext_data['use_location'])) {
				$use_location = $ext_data['use_location'];
			} else {
				$use_location = $this->config->item('default_location_id');
			}

			if (!empty($use_location) AND is_numeric($use_location)) {
				$this->location->setLocation($use_location);
				$data['single_location_url'] = site_url('local?location_id=' . $use_location);
			} else {
				$data['single_location_url'] = site_url('local/all');
			}
		}

		$data['local_action']			= site_url('local_module/local_module/search');

		$data['rsegment'] = $rsegment = ($this->uri->rsegment(1) === 'local_module' AND !empty($this->referrer_uri)) ? $this->referrer_uri : $this->uri->rsegment(1);

		$this->load->library('cart'); 															// load the cart library
		$cart_total = $this->cart->total();

		$data['info_url'] 				= site_url('local');
		$data['local_info'] 			= $this->location->local(); 										// retrieve local location data
		$data['location_id'] 			= $this->location->getId(); 										// retrieve local location data
		$data['location_name'] 			= $this->location->getName(); 										// retrieve local location data
		$data['location_address'] 		= $this->location->getAddress(); 										// retrieve local location data
		$data['location_image'] 		= $this->location->getImage(); 										// retrieve local location data
		$data['is_opened'] 			    = $this->location->isOpened();
		$data['opening_type'] 			= $this->location->workingType('opening');
		$data['opening_status']		 	= $this->location->workingStatus('opening');
		$data['delivery_status']		= $this->location->workingStatus('delivery');
		$data['collection_status']		= $this->location->workingStatus('collection');
		$data['opening_time']		 	= $this->location->workingTime('opening', 'open');
		$data['closing_time'] 			= $this->location->workingTime('opening', 'close');
		$data['order_type']             = $this->location->orderType();
		$data['delivery_charge']        = $this->location->deliveryCharge($cart_total);
		$data['delivery_coverage']      = $this->location->checkDeliveryCoverage();
		$data['search_query']           = $this->location->searchQuery();
		$data['has_search_query']       = $this->location->hasSearchQuery();
		$data['has_delivery']           = $this->location->hasDelivery();
		$data['has_collection']         = $this->location->hasCollection();
		$data['location_order']         = $this->config->item('location_order');

		$data['location_search'] = FALSE;
		if ($rsegment === 'home') {
			$data['location_search'] = TRUE;
		}

		if ($this->config->item('maps_api_key')) {
			$data['map_key'] = '&key='. $this->config->item('maps_api_key');
		} else {
			$data['map_key'] = '';
		}

		$data['delivery_time'] = $this->location->deliveryTime();
		if ($data['delivery_status'] === 'closed') {
			$data['delivery_time'] = 'closed';
		} else if ($data['delivery_status'] === 'opening') {
			$data['delivery_time'] = $this->location->workingTime('delivery', 'open');
		}

		$data['collection_time'] = $this->location->collectionTime();
		if ($data['collection_status'] === 'closed') {
			$data['collection_time'] = 'closed';
		} else if ($data['collection_status'] === 'opening') {
			$data['collection_time'] = $this->location->workingTime('collection', 'open');
		}

		$conditions = array(
			// 'all'   => $this->lang->line('text_condition_all_orders'),
			'above' => $this->lang->line('text_condition_above_total')
			// 'below' => $this->lang->line('text_condition_below_total'),
		);

		$count = 1;
		$data['text_delivery_condition'] = '';
		$delivery_condition = $this->location->deliveryCondition();
		foreach ($delivery_condition as $condition) {
			$condition = explode('|', $condition);

			$delivery = (isset($condition[0]) AND $condition[0] > 0) ? $this->currency->format($condition[0]) : $this->lang->line('text_free_delivery');
			$con = (isset($condition[1])) ? $condition[1] : 'above';
			$total = (isset($condition[2]) AND $condition[2] > 0) ? $this->currency->format($condition[2]) : $this->lang->line('text_no_min_total');

			if ($count === 1 AND isset($condition[0]) AND $condition[0] > 0) {
				$data['text_delivery_condition'] .= sprintf($this->lang->line('text_delivery_charge'), '');
			}

			if ($con === 'all') {
				$data['text_delivery_condition'] .= sprintf($conditions['all'], $delivery);
			} else if ($con === 'above') {
				$data['text_delivery_condition'] .= sprintf($conditions[$con], $delivery, $total) . ', ';
			} else if ($con === 'below') {
				$data['text_delivery_condition'] .= sprintf($conditions[$con], $total) . ', ';
			}

			$count++;
		}

		$data['text_delivery_condition'] = trim($data['text_delivery_condition'], ', ');

		if ($this->location->deliveryCharge($cart_total) > 0) {
			$data['text_delivery_charge'] = sprintf($this->lang->line('text_delivery_charge'), $this->currency->format($this->location->deliveryCharge($cart_total)));
		} else {
			$data['text_delivery_charge'] = $this->lang->line('text_free_delivery');
		}

		if ($this->location->minimumOrder($cart_total) > 0) {
			$data['min_total'] = $this->location->minimumOrder($cart_total);
		} else {
			$data['min_total'] = '0.00';
		}

		$this->load->model('Reviews_model');
		$total_reviews = $this->Reviews_model->getTotalLocationReviews($this->location->getId());
		$data['text_total_review'] = sprintf($this->lang->line('text_total_review'), $total_reviews);
		$data['reviews_query'] = $this->Reviews_model->getLocationReviews($this->location->getId());

		$data['local_alert'] = $this->alert->display('local_module');

		// pass array $data and load view files
		$this->load->view('local_module/local_module', $data);
	}

	
	public function testdata($order_id){
		echo "<pre>";
		$this->load->model('Orders_model');
        $order = $this->Orders_model->getOrder($order_id);
        $orderMenus = $this->Orders_model->getOrderMenus($order_id);
		
		
        //$orderOptions = $this->Orders_model->getMenuOptions($order_id);
	   
	   //$t=unserialize($orderMenus[0]['option_values']);
	   //print_r(unserialize($orderMenus[1]['option_values']));
		
        //print_r(array_keys(unserialize($orderMenus[0]['option_values'])));
		
		//$data['delivery_address'] = $this->country->addressFormat($delivery_address);
		
        $post_array = array();

        $post_array['api_key'] = $this->module['ext_data']['api_key'];
        $post_array['api_password'] = $this->module['ext_data']['api_password'];

        $post_array['receipt_header'] = "Dalgety Fish Bar/rOnline Order%%01383 821599";
        $post_array['receipt_footer'] = "Thank you";

        $post_array['notify_url'] = $this->module['ext_data']['notify_url'];
        $post_array['printer_id'] = $this->module['ext_data']['printer_id'];

        $post_array['order_id'] = $order['order_id'];
        $post_array['currency'] = 'GBP';

        $post_array['order_type'] = $order['order_type'];

        $post_array['payment_status'] = ($order['payment'] == 'cod') ? 7 : 6;
        $post_array['payment_method'] = $order['payment'];

        $post_array['order_time'] = date('H:i', strtotime($order['order_time'])).' '.date('d-m-y', strtotime($order['order_date']));
        $post_array['delivery_time'] = $post_array['order_time'];

        $post_array['deliverycost'] = ($order['order_type'] == 1) ? 2.50 : 0.00;
        $post_array['card_fee'] = 1.50;
        $post_array['total_amount'] = $order['order_total'] + 1.50 + $post_array['deliverycost'];

        $post_array['cust_name'] = $order['first_name'].' '.$order['last_name'];
		
		
        if ($order['address_id'] > 0) {
			$delivery_address = $this->Addresses_model->getAddress($order['customer_id'], $order['address_id']);
            $post_array['cust_address'] = $this->country->addressFormat($delivery_address);
        } else {
            $post_array['cust_address'] = 'No address provided';
        }
        $post_array['cust_phone'] = $order['telephone'];

        //4=verified, 5=not verified
        $post_array['isVarified'] = ($order['customer_id'] > 0) ? 4 : 5;
        $post_array['cust_instruction'] = $order['comment'];
        //$post_array['num_prev_order'] = $num_prev_order;

        $post_array['apply_settings'] = 1;
        $post_array['auto_print'] = 0;
        $post_array['auto_accept'] = 0;
        $post_array['enter_delivery_time'] = 0;
        $post_array['time_input_method'] = 0;
        $post_array['time_list'] = '0-5-10-15-20-25-30-35-40-45-50-55-60';
        $post_array['extra_line_feed'] = 2;

        $cnt = 1;
		print_r($orderMenus);
        foreach($orderMenus as $item){
			$tmp='';
			$ser=unserialize($orderMenus[$cnt-1]['option_values']); 
			//print_r(json_encode($ser[key($ser)]));
			$j=0;
			foreach(array_keys($ser) as $k => $value){
				foreach($ser[$value] as $key=>$value){
					if($j!=0) $tmp.='<br>';
					$j=1;
					$tmp.='  +'.$value['value_name'].' = '.number_format($value['value_price'], 2, '.', '');
				}
			}
			
            $post_array['cat_'.$cnt] = '';
            $post_array['item_'.$cnt] = $item['name'];
            $post_array['option_'.$cnt] = $item['option_name'];
            $post_array['desc_'.$cnt] = $tmp;
            $post_array['qnt_'.$cnt] = $item['quantity'];
            $post_array['price_'.$cnt] = $item['price'];
            $cnt++;
        }
		/* $fields = "";
        $i = 0;
        foreach ($post_array as $key => $value) {
            $fields .= ($i > 0) ? "&" . "$key=" . urlencode($value) : "$key=" . urlencode($value);
            $i++;
        };
        print_r($fields);  */
		print_r($post_array);

	}
	public function search() {
		$this->load->library('user_agent');
		$json = array();

		$result = $this->location->searchRestaurant($this->input->post('search_query'));

		switch ($result) {
			case 'FAILED':
				$json['error'] = $this->lang->line('alert_unknown_error');
				break;
			case 'NO_SEARCH_QUERY':
				$json['error'] = $this->lang->line('alert_no_search_query');
				break;
			case 'INVALID_SEARCH_QUERY':
				$json['error'] = $this->lang->line('alert_invalid_search_query');    // display error: enter postcode
				break;
			case 'outside':
				$json['error'] = $this->lang->line('alert_no_found_restaurant');    // display error: no available restaurant
				break;
		}

		$redirect = '';
		if (!isset($json['error'])) {
			$redirect = $json['redirect'] = site_url('local?location_id='.$this->location->getId());
		}

		if ($redirect === '') {
			$redirect = $this->referrer_uri;
		}

		if ($this->input->is_ajax_request()) {
			$this->output->set_output(json_encode($json));											// encode the json array and set final out to be sent to jQuery AJAX
		} else {
			if (isset($json['error'])) $this->alert->set('custom', $json['error'], 'local_module');
			redirect($redirect);
		}
	}
}

/* End of file local_module.php */
/* Location: ./extensions/local_module/controllers/local_module.php */