<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Remote_print_module extends Main_Controller {

    public function index() {
        $this->lang->load('remote_print_module/remote_print_module');
    }

    public function after_create_order($order_id) {
        $this->module = $this->extension->getModule('remote_print_module');
        $this->sendToPrinter($this->preparePayload($order_id['order_id']));
    }

    private function sendToPrinter($data) {
		

        set_time_limit(60);
        $output = array();

        $fields = "";
        $i = 0;
        foreach ($data as $key => $value) {
            $fields .= ($i > 0) ? "&" . "$key=" . urlencode($value) : "$key=" . urlencode($value);
            $i++;
        };

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $this->module['ext_data']['order_url']);
        curl_setopt($curlSession, CURLOPT_HEADER, 0);
        curl_setopt($curlSession, CURLOPT_POST, 1);
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlSession, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 1);

        $rawresponse = curl_exec($curlSession);

        $response_array = simplexml_load_string($rawresponse);

        if($response_array->status == 'OK'){
            $output['status'] = 'OK';
            $output['details'] = (string)$response_array->details->msg;
        }
        else{
            $output['status'] = 'FAILED';
            $output['error'] = array();

            foreach($response_array->details->error as $val) {
                $output['error'][] = (string)$val;
            }
        }

        curl_close($curlSession);

        if($output['status']=='OK'){
            redirect('checkout/success');
            //order submitted successfully
        }
        else{
            //order failed because of following reason
            print_r($output['error']);
        }
    }

    private function preparePayload($order_id) {
        $this->load->model('Orders_model');
		$this->load->model('Addresses_model'); 
		$this->load->library('country');
        $order = $this->Orders_model->getOrder($order_id);
        $orderMenus = $this->Orders_model->getOrderMenus($order_id);
        // $has_delivery = $this->location->hasDelivery();
        $delivery_charge_cost = $this->location->deliveryCharge($cart_total);
       // $orderOptions = $this->Orders_model->getMenuOptions($order_id);
        //print_r($order);

        $post_array = array();

        $post_array['api_key'] = $this->module['ext_data']['api_key'];
        $post_array['api_password'] = $this->module['ext_data']['api_password'];

        $post_array['receipt_header'] = "Dalgety Fish Bar App Order";
        $post_array['receipt_footer'] = "Thank you";

        $post_array['notify_url'] = $this->module['ext_data']['notify_url'];
        $post_array['printer_id'] = $this->module['ext_data']['printer_id'];

        $post_array['order_id'] = $order['order_id'];
        $post_array['currency'] = 'GBP';

        $post_array['order_type'] = $order['order_type'];

        $post_array['payment_status'] = ($order['payment'] == 'cod') ? 7 : 6;
        $post_array['payment_method'] = $order['payment'];

        $post_array['order_time'] = date('H:i d-m-y');
        $post_array['delivery_time'] = date('H:i', strtotime($order['order_time'])).' '.date('d-m-y', strtotime($order['order_date']));

        if($order['order_time_type'] == 'asap'){
            $post_array['delivery_time'] .= ' _ ASAP';
        }

        if($order['order_type'] == 1){
            $post_array['deliverycost'] = $delivery_charge_cost; //($order['order_type'] == 1) ? 2.50 : 0.00;
        }else{
            $post_array['deliverycost'] = 0.00;
        }

        $post_array['card_fee'] = 1.00;
        $post_array['total_amount'] = $order['order_total']; //+ $post_array['deliverycost'];

        $post_array['cust_name'] = $order['first_name'].' '.$order['last_name'];
        if ($order['address_id'] > 0) {
            $delivery_address = $this->Addresses_model->getAddress($order['customer_id'], $order['address_id']);
            $post_array['cust_address'] = $this->country->addressFormat($delivery_address);
        } else {
            $post_array['cust_address'] = 'Pick Up';
        }
        $post_array['cust_phone'] = $order['telephone'];

        //4=verified, 5=not verified
        $post_array['isVarified'] = ($order['customer_id'] > 0) ? 4 : 5;
        $post_array['cust_instruction'] = $order['comment'];
        //$post_array['num_prev_order'] = $num_prev_order;

        $post_array['apply_settings'] = 1;
        $post_array['auto_print'] = 0;
        $post_array['auto_accept'] = 0;
        $post_array['enter_delivery_time'] = 0;
        $post_array['time_input_method'] = 0;
        $post_array['time_list'] = '0-5-10-15-20-25-30-35-40-45-50-55-60';
        $post_array['extra_line_feed'] = 2;

        $cnt = 1;
        foreach($orderMenus as $item){
			$tmp='';
			$ser=unserialize($orderMenus[$cnt-1]['option_values']); 
			//print_r(json_encode($ser[key($ser)]));
			/* foreach($ser[key($ser)] as $key=>$value){
				$tmp.='<br>  +'.$value['value_name'].' '.$value['value_price'];
			} 
			foreach(array_keys($ser) as $k => $value){
				foreach($ser[$value] as $key=>$value){
					$tmp.='<br>  +'.$value['value_name'].' = '.number_format($value['value_price'], 2, '.', '');
				}
			}*/
			$j=0;
			foreach(array_keys($ser) as $k => $value){
				foreach($ser[$value] as $key=>$value1){
					if($j!=0) $tmp.='<br>';
					$j=1;
					$tmp.='  +'.$value1['value_name'].' = '.number_format($value1['value_price'], 2, '.', '');
				}
			}

			if($item['comment']){
                $tmp.='<br>[';
                $tmp.= $item['comment'];
                $tmp .= ']';
            }

            $post_array['cat_'.$cnt] = '';
            $post_array['item_'.$cnt] = $item['name'];
            $post_array['option_'.$cnt] = $item['option_name'];
            $post_array['desc_'.$cnt] = $tmp;
            $post_array['qnt_'.$cnt] = $item['quantity'];
            $post_array['price_'.$cnt] = $item['price'];
            $cnt++;
        }

        return $post_array;
    }
}

/* End of file remote_print_module.php */
/* Location: ./extensions/stripe/controllers/remote_print_module.php */