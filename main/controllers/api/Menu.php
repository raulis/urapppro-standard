<?php if (!defined('BASEPATH')) exit('No direct access allowed');

class Menu extends Main_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Categories_model');
        $this->load->model('Menu_options_model');
        $this->load->model('Menus_model');
        $this->load->model('Image_tool_model');
        $this->load->library('currency');
    }

    public function testStatistics()
    {
//        $this->response->addHeader('Content-Type: application/json');
//        $data = $this->input->get('product');
        $data = $this->input->get('product');
        $this->output->set_output(json_encode($data));
    }

    public function getAllCategories()
    {
        $categories = $this->Categories_model->getCategories();
        $this->output->set_output(json_encode($categories));
    }

    public function getMenuByCategory()
    {
        $category_id = $this->input->get('category_id');
        $categories = $this->Categories_model->getCategory($category_id);
        if (!$categories AND $category_id) {
            show_404();
        }

        $filter = array();
        $filter['page'] = "";
        $filter['limit'] = "200";
        $filter['sort_by'] = 'menus.menu_priority';
        $filter['order_by'] = 'ASC';
        $filter['filter_status'] = '1';
        $filter['filter_category'] = $category_id;

        $category_menus = $this->Menus_model->getList($filter);

        $this->output->set_output(json_encode($category_menus));
    }

    public function getMenuOptions()
    {
        $list_data['menu_options'] = array();
        $menu_options = $this->Menu_options_model->getMenuOptions();

        foreach ($menu_options as $menu_id => $option) {
            $option_values = array();

            foreach ($option['option_values'] as $value) {
                $option_values[] = array(
                    'option_value_id' => $value['option_value_id'],
                    'value' => $value['value'],
                    'price' => (empty($value['new_price']) OR $value['new_price'] == '0.00') ? $this->currency->format($value['price']) : $this->currency->format($value['new_price']),
                );
            }

            $list_data['menu_options'][$option['menu_id']][] = array(
                'menu_option_id' => $option['menu_option_id'],
                'option_id' => $option['option_id'],
                'option_name' => $option['option_name'],
                'display_type' => $option['display_type'],
                'priority' => $option['priority'],
                'default_value_id' => isset($option['default_value_id']) ? $option['default_value_id'] : 0,
                'option_values' => $option_values,
            );
        }

        $list_data['option_values'] = array();
        foreach ($menu_options as $option) {
            if (!isset($list_data['option_values'][$option['option_id']])) {
                $list_data['option_values'][$option['option_id']] = $this->Menu_options_model->getOptionValues($option['option_id']);
            }
        }

        $this->output->set_output(json_encode($list_data));

    }

    public function tableUpdateTime($tableName)
    {
        $query = $this->db->query("SELECT UPDATE_TIME
FROM information_schema.tables
WHERE TABLE_SCHEMA =  'fishbar'
AND TABLE_NAME =  '" . $tableName . "'");

        $s_r = $query->row();

        return isset($s_r) ? strtotime($s_r->UPDATE_TIME) : '';
    }

    public function getUpdateTime()
    {
        $update_time['categories_update_time'] = $this->tableUpdateTime('1ka85sjpv_categories');

        $update_time['menus_update_time'] = $this->tableUpdateTime('1ka85sjpv_menus');

        $update_time['options_update_time'] = $this->tableUpdateTime('1ka85sjpv_options');

        $this->output->set_output(json_encode($update_time));
    }

    public function getAll()
    {

        $filter = array();
        $filter['page'] = "";
        $filter['limit'] = "200";
        $filter['sort_by'] = 'menus.menu_priority';
        $filter['order_by'] = 'ASC';
        $filter['filter_status'] = '1';
        $filter['filter_category'] = '';

        $list_data['menus'] = $this->Menus_model->getList($filter);

        $categories = $this->Categories_model->getCategories();

        $list_data['categories'] = array();
        foreach (sort_array($categories) as $category) {
            if (!empty($filter['filter_category']) AND $filter['filter_category'] != $category['category_id']) continue;

            $category_image = '';
            if (!empty($category['image'])) {
                $category_image = $this->Image_tool_model->resize($category['image'], '800', '115');
            }

            $list_data['categories'][$category['category_id']] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'description' => $category['description'],
                'priority' => $category['priority'],
                'child' => $category['child_id'],
                'parent' => $category['parent_id'],
                'image' => $category_image
            );
        }

        foreach ($list_data['categories'] as $category) {
            if ($category['child'] !== null) {
                $category['child'] = null;
                foreach ($list_data['categories'] as $sub_category) {
                    if ($sub_category['parent'] === $category['category_id'] && $category['category_id'] !== $sub_category['category_id']) {
                        $category['child'][$sub_category['category_id']] = $sub_category;
                        $list_data['categories'][$category['category_id']] = $category;
                        unset($list_data['categories'][$sub_category['category_id']]);
                    }
                }
            }
        }

        $list_data['menu_options'] = array();
        $menu_options = $this->Menu_options_model->getMenuOptions();
        foreach ($menu_options as $menu_id => $option) {
            $option_values = array();

            foreach ($option['option_values'] as $value) {
                $option_values[] = array(
                    'option_value_id' => $value['option_value_id'],
                    'value' => $value['value'],
                    'price' => (empty($value['new_price']) OR $value['new_price'] == '0.00') ? $this->currency->format($value['price']) : $this->currency->format($value['new_price']),
                );
            }

            $list_data['menu_options'][$option['menu_id']][] = array(
                'menu_option_id' => $option['menu_option_id'],
                'option_id' => $option['option_id'],
                'option_name' => $option['option_name'],
                'display_type' => $option['display_type'],
                'priority' => $option['priority'],
                'default_value_id' => isset($option['default_value_id']) ? $option['default_value_id'] : 0,
                'option_values' => $option_values,
            );
        }

        $list_data['option_values'] = array();
        foreach ($menu_options as $option) {
            if (!isset($list_data['option_values'][$option['option_id']])) {
                $list_data['option_values'][$option['option_id']] = $this->Menu_options_model->getOptionValues($option['option_id']);
            }
        }

        $this->output->set_output(json_encode($list_data));
    }

}
