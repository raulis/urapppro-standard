<?php echo get_header(); ?>
<?php echo get_partial('content_top'); ?>
    <div id="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php echo load_partial('local_reviews', $local_reviews); ?>
                </div>
                <?php echo get_partial('content_bottom'); ?>
            </div>
        </div>
    </div>
<?php echo get_footer(); ?>