<?php

	$pages = $this->Pages_model->getPages();

	$body_class = '';
	if ($this->uri->rsegment(1) === 'menus') {
		$body_class = 'menus-page';
	} else if ($this->uri->rsegment(1) === 'home') {
	    $body_class = 'home-page-body';
	}
?>
<?php echo get_doctype(); ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" style="overflow-x: hidden;">
	<head>
        <?php echo get_metas(); ?>
        <?php if ($favicon = get_theme_options('favicon')) { ?>
            <link href="<?php echo image_url($favicon); ?>" rel="shortcut icon" type="image/ico">
        <?php } else { ?>
            <?php echo get_favicon(); ?>
        <?php } ?>
<!--        <title>Dalgety Bay Fish Bar - Fish and Chips in Dalgety Bay, Dunfermline, Fife</title>-->
        <title><?php echo sprintf(lang('site_title'), get_title(), config_item('site_name')); ?></title>
        <?php echo get_style_tags(); ?>
        <?php echo get_active_styles(); ?>
        <?php echo get_script_tags(); ?>
        <?php echo get_theme_options('ga_tracking_code'); ?>
		<script type="text/javascript">
			var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

			var js_site_url = function(str) {
				var strTmp = "<?php echo rtrim(site_url(), '/').'/'; ?>" + str;
			 	return strTmp;
			};

			var js_base_url = function(str) {
				var strTmp = "<?php echo base_url(); ?>" + str;
				return strTmp;
			};

            var pageHeight = $(window).height();

			$(document).ready(function() {
				if ($('#notification > p').length > 0) {
					setTimeout(function() {
						$('#notification > p').slideUp(function() {
							$('#notification').empty();
						});
					}, 3000);
				}

				$('.alert').alert();
				$('.dropdown-toggle').dropdown();
                $('a[title], i[title]').tooltip({placement: 'bottom'});
                $('select.form-control').select2();
			});
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        <?php $custom_script = get_theme_options('custom_script'); ?>
        <?php if (!empty($custom_script['head'])) { echo '<script type="text/javascript">'.$custom_script['head'].'</script>'; }; ?>
	</head>
	<body class="<?php echo $body_class; ?>">
		<div id="opaclayer" onclick="closeReviewBox();"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe"><?php echo lang('alert_info_outdated_browser'); ?></p>
        <![endif]-->

		<header id="main-header">
			<div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="logo pull-left">
                            <a class="header-back"
                                href="<?php
                                    $current_url = explode('/', trim(str_replace(site_url(), '', page_url()), '/'));
                                    $redirect_url = '';
                                    if(count($current_url) > 0) {
                                        if($current_url[0] === 'about-us' || ($current_url[0] === 'account' && count($current_url) === 1))
                                            $redirect_url = $redirect_url . 'local?location_id=11';
                                        else
                                            for ($i = 0; $i < count($current_url) - 1; $i++)
                                                $redirect_url = '/' . $redirect_url . $current_url[$i];
                                    }
                                    echo site_url($redirect_url);
                                ?>">
                                <i id="header-back-icon" class="fa fa-chevron-left header-back" aria-hidden="true"></i>
							</a>

							<a href="<?php echo site_url(); ?>" style="margin-left: 8vw;"><i class="fa fa-home header-button"></i></a>
						</div>
						<div class="cart-div pull-right">
						    <a id="header-cart-button" class="btn btn-primary" href="<?php echo rtrim(site_url(), '/').'/cart'; ?>" style="text-overflow:ellipsis; overflow:hidden;">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;<span id="header-cart-text">£<?php echo $this->cart->total() ? number_format((float)$this->cart->total(), 2, '.', ',') : '0.00'; ?></span>
                            </a>
                        </div>
					</div>
				</div>
			</div>
		</header>

		<nav id="sidebar" class="navbar navbar-default side-menu right-slide-menu" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <span class="navbar-trigger-close sidebar-close">
                        <a href="#">&times;</a>
                    </span>

                    <a class="navbar-brand" href="#">
                        <!--<img src="{{ asset('/img/colour-logo.png') }}">-->
                    </a>
                </div>

                <div style="height: 60px;"></div>

                <div class="list-group text-center">
                    <a class="list-group-item" href="<?php echo site_url('account/details'); ?>">
                        <span class="sidebar-menu-item">My Details</span>
                    </a>
                    <a class="list-group-item" href="<?php echo site_url('account/address'); ?>">
                        <span class="sidebar-menu-item">Address Book</span>
                    </a>
                    <a class="list-group-item" href="<?php echo site_url('account/orders'); ?>">
                        <span class="sidebar-menu-item">Orders</span>
                    </a>
                    <a class="list-group-item" href="<?php echo site_url('reviews'); ?>">
                        <span class="sidebar-menu-item">Reviews</span>
                    </a>
                    <a class="list-group-item" href="<?php echo site_url('contact'); ?>">
                        <span class="sidebar-menu-item">Contact Us</span>
                    </a>
                </div>

                <?php if($this->customer->isLogged()) { ?>
                    <div class="panel-post">
                        <div class="alert alert-success text-center">
                            <?php echo 'Logged in as <b>' . $this->customer->getFirstName() . ' ' . $this->customer->getLastName() . '</b>'; ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-warning text-center" role="alert">You are not logged in. Please log in to take full advantage of the app features.</div>
                            <div class="form-group">
                                <a href="<?php echo site_url('login') ?>" class="btn btn-primary btn-lg" style="width:100%">Login</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
		</nav>

		<div id="page-wrapper" class="content-area">
		    <?php
                $current_url = explode('/', trim(str_replace(site_url(), '', page_url()), '/'));
                if(count($current_url) > 0)
                    if($current_url[0] !== '')
                        echo '<div class="hidden-md hidden-sm hidden-lg" style="margin-top: 60px;"></div>';
		    ?>
			<?php if (get_theme_options('display_crumbs') === '1' AND ($breadcrumbs = get_breadcrumbs()) !== '') { ?>
	            <div id="breadcrumb">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
                                <?php echo $breadcrumbs; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>

            <?php if (($page_heading = get_heading()) !== '') { ?>
	            <div id="heading">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
	                            <div class="heading-content">
	                                <h2><?php echo $page_heading; ?></h2>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>